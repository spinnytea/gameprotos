'use strict';
var _ = require('../../../../lodashAdapter');

module.exports = State;
function State(config) {
  _.merge(this, {
    animation: {
      speed: 1,

      thigh: { amp: 0, off: 0 },
      calf: { amp: 0, off: 0 },

      upperarm: { amp: 0, off: 0 },
      forearm: { amp: 0, off: 0 },

      head: { myR: 0 },
      back: { myR: 0 }
    } // end animation
  }, config);
}