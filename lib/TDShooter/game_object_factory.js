'use strict';
var _ = require('../lodashAdapter');

// create a new object for the registry
// this is a bag of behaviors
// behaviors: { service: config, service: config }
exports.construct = function(behaviors) {
  return {
    $behavior: _.map(behaviors, function(value, key) {
      return { service: key, config: value };
    })
  };
};

exports.player = function() {
  return exports.construct({
    'gameprotos.tdshooter.behaviors.game_object': {
      type: 'players',
      health: 20,

      deathSound: true,

      x: 900,
      y: 600,
      r: -Math.PI/2,
      radius: 50,
      image: 'player.ship'
    },
    'gameprotos.tdshooter.behaviors.death': undefined,
    'gameprotos.tdshooter.behaviors.collide': ['enemyBullets'],
    'gameprotos.tdshooter.behaviors.bounds.boxed': undefined,
    'gameprotos.tdshooter.behaviors.player_keys': undefined
  });
};

exports.player.bullet = function(p) {
  return exports.construct({
    'gameprotos.tdshooter.behaviors.game_object': {
      type: 'playerBullets',
      health: 2,

      x: p.x,
      y: p.y,
      r: p.r,
      radius: 20,
      image: 'player.bullet'
    },
    'gameprotos.tdshooter.behaviors.death': undefined,
    'gameprotos.tdshooter.behaviors.projectile': undefined,
    'gameprotos.tdshooter.behaviors.bounds.vertical': undefined
  });
};

exports.enemy = function(config) {
  return exports.construct({
    'gameprotos.tdshooter.behaviors.game_object': _.merge({
      type: 'enemies',
      health: 3,

      x: 200,
      y: 200,
      r: Math.PI/2,
      radius: 50,
      image: 'enemy.ship'
    }, config),
    'gameprotos.tdshooter.behaviors.death': undefined,
    'gameprotos.tdshooter.behaviors.collide': ['playerBullets', 'players'],
    'gameprotos.tdshooter.behaviors.enemy.random_fire': undefined,
    'gameprotos.tdshooter.behaviors.bounds.boxed': undefined,
    'gameprotos.tdshooter.behaviors.enemy.wander': undefined
  });
};

exports.enemy.bullet = function(e) {
  return exports.construct({
    'gameprotos.tdshooter.behaviors.game_object': {
      type: 'enemyBullets',
      health: 1,

      x: e.x,
      y: e.y,
      r: e.r,
      radius: 20,
      image: 'enemy.bullet'
    },
    'gameprotos.tdshooter.behaviors.death': undefined,
    'gameprotos.tdshooter.behaviors.projectile': undefined,
    'gameprotos.tdshooter.behaviors.bounds.vertical': undefined
  });
};