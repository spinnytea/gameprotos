'use strict';
module.exports = require('angular').module('gameprotos.2dracing', [
  require('./behaviors'),
  require('./stages')
]).name;