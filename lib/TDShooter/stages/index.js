'use strict';
module.exports = require('angular').module('gameprotos.tdshooter.stages', [
  require('./loadingProgram'),
  require('./stage1'),
  require('./stage2'),
  require('./stage3')
]).name;