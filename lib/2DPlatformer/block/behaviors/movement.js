'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');
var BLOCK_SIZE = require('../game_object_factory').BLOCK_SIZE;
var INNER_COLLIDE = BLOCK_SIZE*0.8;

module.exports = require('angular').module('gameprotos.2dplatformer.block.behaviors.movement', [])
  .service('gameprotos.2dplatformer.block.behaviors.movement', Movement).name;

function Movement() {
  Behavior.call(this);
}
util.inherits(Movement, Behavior);

Movement.prototype.init = function(obj, f5, config) {
  _.merge(obj, {
    controls: {
      left: false,
      right: false,
      jump: false
    },
    movement: {
      maxJump: 70,
      gravity: 120,
      jump: 0,
      canJump: false,

      lr_speed: 1500
    }
  }, config);
};

Movement.prototype.update = function(obj, f5, dt) {
  dt /= 1e3;

  var dy = 0;
  var dx = 0;

  if(obj.controls.left)
    dx -= obj.movement.lr_speed*dt;
  if(obj.controls.right)
    dx += obj.movement.lr_speed*dt;
  if(obj.controls.jump && obj.movement.canJump) {
    obj.movement.jump = -obj.movement.maxJump;
    obj.movement.canJump = false;
  }

  obj.movement.jump += obj.movement.gravity*dt;
  dy += obj.movement.jump;

  obj.y += dy;
  obj.x += dx;

  f5.registry.blocks.forEach(function(block) {
    // if I am colliding with any block, place it to the side

    if(obj.x < block.x + BLOCK_SIZE && obj.x + BLOCK_SIZE > block.x &&
      obj.y < block.y + BLOCK_SIZE && obj.y + BLOCK_SIZE > block.y) {

      if(obj.x < block.x + INNER_COLLIDE && obj.x + INNER_COLLIDE > block.x) {
        if(obj.y < block.y) {
          obj.y = block.y - BLOCK_SIZE;
          obj.movement.canJump = true;
        }
        if(obj.y > block.y)
          obj.y = block.y + BLOCK_SIZE;

        obj.movement.jump = 0;
      }

      if(obj.y < block.y + INNER_COLLIDE && obj.y + INNER_COLLIDE > block.y) {
        if(obj.x < block.x)
          obj.x = block.x - BLOCK_SIZE;
        if(obj.x > block.x)
          obj.x = block.x + BLOCK_SIZE;
      }

    }

  });
};