'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.tdshooter.behaviors.enemy.wander', [])
  .service('gameprotos.tdshooter.behaviors.enemy.wander', Wander).name;

function Wander() {
  Behavior.call(this);
}
util.inherits(Wander, Behavior);

Wander.prototype.init = function(obj, f5, config) {
  _.merge(obj, { wander: _.merge({
    r: 0,
    timeout_max: 1,
    timeout_curr: 0,
    speed: 200
  }, config) });
};

Wander.prototype.update = function(obj, f5, dt) {
  dt /= 1e3;

  obj.wander.timeout_curr -= dt;
  if(obj.wander.timeout_curr < 0) {
    obj.wander.r = Math.random()*Math.PI*2;
    obj.wander.timeout_curr = obj.wander.timeout_max;
  }

  obj.x += Math.cos(obj.wander.r) * obj.wander.speed * dt;
  obj.y += Math.sin(obj.wander.r) * obj.wander.speed * dt;
};