'use strict';
module.exports = require('angular').module('gameprotos.tdshooter', [
  require('./behaviors'),
  require('./stages')
]).name;