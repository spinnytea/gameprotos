'use strict';
var _ = require('../../lodashAdapter');

var BLOCK_SIZE = exports.BLOCK_SIZE = 500;
var BLOCK_SPACING = exports.BLOCK_SPACING = BLOCK_SIZE + 50;

// create a new object for the registry
// this is a bag of behaviors
// behaviors: { service: config, service: config }
exports.construct = function(behaviors) {
  return {
    $behavior: _.map(behaviors, function(value, key) {
      return { service: key, config: value };
    })
  };
};

exports.block = function(x, y, image) {
  return exports.construct({
    'gameprotos.2dplatformer.block.behaviors.game_object': {
      x: x*BLOCK_SPACING,
      y: y*BLOCK_SPACING,

      sprite: {
        offset_x: 50+125,
        offset_y: 50+50,
        width: 850,
        height: 700,
        image: image
      }
    }
  });
};

exports.player = function(x, y, image, keys) {
  return exports.construct({
    'gameprotos.2dplatformer.block.behaviors.game_object': {
      x: x*BLOCK_SPACING,
      y: y*BLOCK_SPACING,

      sprite: {
        offset_x: 50,
        offset_y: 50,
        width: 600,
        height: 600,
        image: image
      }
    },
    'gameprotos.2dplatformer.block.behaviors.keyboard': keys,
    'gameprotos.2dplatformer.block.behaviors.movement': undefined
  });
};