'use strict';
var State = require('./player_state');

module.exports = new State({
  animation: {
    speed: 3.5,

    thigh: { amp: 0.9, off: -0.3 },
    calf: { amp: 0.4, off: 0.9 },

    upperarm: { amp: 1, off: 0 },
    forearm: { amp: 0.4, off: 0 },

    head: { myR: 0 },
    back: { myR: 0 }
  }
});