'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.tdshooter.behaviors.death', [])
  .service('gameprotos.tdshooter.behaviors.death', Death).name;

function Death() {
  Behavior.call(this);
  this.$priority = 200;
}
util.inherits(Death, Behavior);

Death.prototype.update = function(obj, f5) {
  if(obj.health <= 0) {
    f5.registry[obj.type].splice(f5.registry[obj.type].indexOf(obj), 1);
    if(obj.deathSound)
      f5.resource.sound['death'].play();
  }
};
