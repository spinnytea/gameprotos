'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.loadingProgram', [])
  .service('gameprotos.2dplatformer.ragdoll.loadingProgram', PStage).name;

function PStage() {
  Stage.call(this);
}
util.inherits(PStage, Stage);

PStage.prototype.init = function(f5) {
  f5.resource.loadImage('platform', '/resource/2DPlatformer/ragdoll/platform.png');
  f5.resource.loadImage('swatch', '/resource/swatch.png');
  f5.resource.loadImage('dot', '/resource/dot.png');

  f5.resource.loadImage('player.head', '/resource/2DPlatformer/ragdoll/player/head.png');
  f5.resource.loadImage('player.body', '/resource/2DPlatformer/ragdoll/player/body.png');
  f5.resource.loadImage('player.forearm', '/resource/2DPlatformer/ragdoll/player/forearm.png');
  f5.resource.loadImage('player.upperarm', '/resource/2DPlatformer/ragdoll/player/upperarm.png');
  f5.resource.loadImage('player.thigh', '/resource/2DPlatformer/ragdoll/player/thigh.png');
  f5.resource.loadImage('player.calf', '/resource/2DPlatformer/ragdoll/player/calf.png');
};

PStage.prototype.enter = function(f5) {
  f5.changeStage('gameprotos.2dplatformer.ragdoll.stages.stage1');
};