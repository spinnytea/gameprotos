'use strict';
module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.behaviors', [
  require('./animate'),
  require('./collision/box'),
  require('./collision/point'),
  require('./game_object'),
  require('./keyboard_control'),
  require('./movement')
]).name;