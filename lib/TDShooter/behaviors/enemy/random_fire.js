'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');
var factory = require('../../game_object_factory');

module.exports = require('angular').module('gameprotos.tdshooter.behaviors.enemy.random_fire', [])
  .service('gameprotos.tdshooter.behaviors.enemy.random_fire', RandomFire).name;

function RandomFire() {
  Behavior.call(this);
}
util.inherits(RandomFire, Behavior);

RandomFire.prototype.init = function(obj, f5, config) {
  _.merge(obj, { fire: _.merge({
    charge: Math.random() * 2,
    delay: 2
  }, config) });
};

RandomFire.prototype.update = function(obj, f5, dt) {
  obj.fire.charge += dt/1e3;
  if(Math.random() * obj.fire.charge > obj.fire.delay) {
    obj.fire.charge = 0;
    f5.registry.enemyBullets.push(factory.enemy.bullet(obj));
    f5.resource.sound['enemy.fire'].play();
  }
};