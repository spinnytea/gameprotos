'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dracing.behaviors.car.collision', [])
  .service('gameprotos.2dracing.behaviors.car.collision', Collision).name;

function Collision() {
  Behavior.call(this);
}
util.inherits(Collision, Behavior);

Collision.prototype.update = function(obj, f5) {
  f5.registry.cars.forEach(function(c) {
    // car doesn't collide with itself
    if(c === obj) return;

    if(distance(c, obj) < c.radius + obj.radius) {
      var r = Math.atan2(c.y - obj.y, c.x - obj.x);

      c.boostX += Math.cos(r) * obj.boostCollision;
      c.boostY += Math.sin(r) * obj.boostCollision;

      obj.boostX -= Math.cos(r) * c.boostCollision;
      obj.boostY -= Math.sin(r) * c.boostCollision;
    }
  });
};

function distance(o1, o2) {
  return Math.sqrt(Math.pow(o1.x - o2.x, 2) + Math.pow(o1.y - o2.y, 2));
}