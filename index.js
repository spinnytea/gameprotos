'use strict';
require('angular').module('gameprotos', [
  require('force5'),
  require('./lib/loadingProgram'),
  require('./lib/2DRacing'),
  require('./lib/TDShooter'),
  require('./lib/2DPlatformer/block'),
  require('./lib/2DPlatformer/ragdoll')
]);
