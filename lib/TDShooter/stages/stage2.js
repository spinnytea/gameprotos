'use strict';
var util = require('util');
var PStage = require('./pstage');
var factory = require('../game_object_factory');

module.exports = require('angular').module('gameprotos.tdshooter.stages.stage2', [])
  .service('gameprotos.tdshooter.stages.stage2', Stage).name;

function Stage() {
  PStage.call(this, 'gameprotos.tdshooter.stages.stage3');
}
util.inherits(Stage, PStage);

Stage.prototype.enter = function(f5) {
  var total = f5.registry.players.reduce(function(sum, p) {
    return sum + p.health;
  }, 0);

  for(var i=0; i<total; i++)
    f5.registry.enemies.push(factory.enemy({
      x: Math.random()*f5.width,
      y: Math.random()*f5.height/2
    }));
};
