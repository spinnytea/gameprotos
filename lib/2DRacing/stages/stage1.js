'use strict';
var util = require('util');
var PStage = require('./pstage');
var factory = require('../game_object_factory');

module.exports = require('angular').module('gameprotos.2dracing.stages.stage1', [])
  .service('gameprotos.2dracing.stages.stage1', Stage).name;

function Stage() {
  PStage.call(this);
}
util.inherits(Stage, PStage);

Stage.prototype.enter = function(f5) {
  var colors = ['car.red', 'car.blue', 'car.green', 'car.purple', 'car.orange', 'car.pink'];
  var color = colors.splice(Math.floor(Math.random()*colors.length), 1)[0];

  // NPCs go first
  f5.registry.cars = colors.map(function(c, idx) {
    return factory.car({
      place: idx,
      startX: 900,
      startY: 140,
      image: c
    });
  });

  // add player
  f5.registry.cars.push(factory.car({
    place: f5.registry.cars.length,
    startX: 900,
    startY: 140,
    image: color,
    keys: {
      left: 'left',
      right: 'right',
      gas: 'up',
      break: 'down'
    }
  }));

  f5.registry.track = factory.track([
    { x: 0, y: 0 },
    { x: 6, y: 0 },
    { x: 6, y: 3 },

    // detour
    { x: 4, y: 3 },
    { x: 4, y: 2 },
    { x: 2, y: 2 },
    { x: 2, y: 3 },

    { x: 0, y: 3 }
  ]);

  // TODO load bushes surrounding track
  // - record a matrix of positions (sparse list)
  // - fill -1, +1 and non-track with pushes
};
