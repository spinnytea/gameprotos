'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

module.exports = require('angular').module('gameprotos.loadingProgram', [])
  .service('gameprotos.loadingProgram', PStage).name;

function PStage() {
  Stage.call(this);
  this.templateUrl = '/template/picker.html';
}
util.inherits(PStage, Stage);

PStage.prototype.enter = function() {
  //f5.changeStage('gameprotos.2dracing.loadingProgram');
  //f5.changeStage('gameprotos.tdshooter.loadingProgram');
  //f5.changeStage('gameprotos.2dplatformer.ragdoll.loadingProgram');
  //f5.changeStage('gameprotos.2dplatformer.block.loadingProgram');
};