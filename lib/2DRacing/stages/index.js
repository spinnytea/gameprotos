'use strict';
module.exports = require('angular').module('gameprotos.2dracing.stages', [
  require('./loadingProgram'),
  require('./stage1')
]).name;