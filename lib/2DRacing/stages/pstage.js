'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

module.exports = PStage;

// TODO remove this intermediate class
// - all tracks are generated, so it really isn't needed
function PStage() {
  Stage.call(this);
}
util.inherits(PStage, Stage);

//PStage.prototype.update = function(f5) {
//};
