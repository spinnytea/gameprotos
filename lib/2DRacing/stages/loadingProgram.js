'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

module.exports = require('angular').module('gameprotos.2dracing.loadingProgram', [])
  .service('gameprotos.2dracing.loadingProgram', PStage).name;

function PStage() {
  Stage.call(this);
}
util.inherits(PStage, Stage);

// TODO provide a universal configuration?
// - the movements can be slow and deliberate, super bouncy, or fast and spastic
// - with those configurations, we need to adjust things like the track swatch size
//   - (if they move quickly, the track blocks need to be larger)
PStage.prototype.init = function(f5) {
  f5.resource.loadImage('car.red', '/resource/2DRacing/red.png');
  f5.resource.loadImage('car.blue', '/resource/2DRacing/blue.png');
  f5.resource.loadImage('car.green', '/resource/2DRacing/green.png');
  f5.resource.loadImage('car.purple', '/resource/2DRacing/purple.png');
  f5.resource.loadImage('car.orange', '/resource/2DRacing/orange.png');
  f5.resource.loadImage('car.pink', '/resource/2DRacing/pink.png');

  f5.resource.loadImage('track.straight', '/resource/2DRacing/straight.png');
  f5.resource.loadImage('track.turn', '/resource/2DRacing/turn.png');
  //f5.resource.loadImage('track.sharp_turn', '/resource/2DRacing/sharp_turn.png');

  // TODO loadSound
  // - engine
  // - turn
  // - bump
  // - break
  // - Music

  // TODO update view based on car locations
  f5.view = {
    x: 0,
    y: 0,
    width: 1920,
    height: 1080
  };
};

PStage.prototype.enter = function(f5) {
  f5.changeStage('gameprotos.2dracing.stages.stage1');
};