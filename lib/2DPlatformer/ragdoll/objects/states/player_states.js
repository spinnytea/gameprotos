'use strict';

exports.stand = require('./player_stand');
exports.frolic = require('./player_frolic');
exports.walk = require('./player_walk');
exports.run = require('./player_run');
exports.jump = require('./player_jump');
exports.fall = require('./player_fall');
