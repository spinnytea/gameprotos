'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dracing.behaviors.car.movement', [])
  .service('gameprotos.2dracing.behaviors.car.movement', CarMovement).name;

function CarMovement() {
  Behavior.call(this);
}
util.inherits(CarMovement, Behavior);

CarMovement.prototype.init = function(obj, f5, config) {
  // TODO balance parameters
  _.merge(obj, {
    controls: {
      left: false,
      right: false,
      gas: false,
      break: false
    },

    // max:
    //  the max value this can reach (or min = -max when negative is allowed)
    // dec:
    //  how much drag is there on the car; how much it slows down when you aren't pushing forward
    //  this is percent per second
    // break:
    //  save as dec; applied when breaking


    // jerk
    j: 10000,

    // acceleration
    a: 0,
    dragA: 2,
    maxA: 800,

    // velocity
    v: 0,
    dragV: 0.7,
    breakV: 3,
    offTrackV: 4,
    maxV: 400,


    // boost
    boostX: 0,
    boostY: 0,
    dragBoost: 4,
    boostCollision: 200, // sort of like a directed velocity when you collide with something


    // turn (basically a velocity; acceleration is fixed (no jolt))
    turn: 0,
    turnDec: 5, // this will take 1/turnDec seconds to slow down to nothing
    turnAcc: Math.PI*6,
    maxTurn: Math.PI
  }, config);
};

CarMovement.prototype.update = function(obj, f5, dt) {
  dt /= 1e3;

  //
  // handle controls
  //

  // keep turn bounded to min/max
  if(obj.controls.left) {
    obj.turn = Math.max(-obj.maxTurn, obj.turn - obj.turnAcc*dt);
  } else if(obj.controls.right) {
    obj.turn = Math.min(obj.maxTurn, obj.turn + obj.turnAcc*dt);
  } else {
    // apply drag to turn
    obj.turn *= 1 - obj.turnDec*dt;
  }

  if(obj.controls.gas) {
    obj.a = Math.min(obj.maxA, obj.a + obj.j*dt);
  } else {
    // apply drag to a
    obj.a *= 1 - obj.dragA*dt;
  }

  if(obj.controls.break) {
    obj.a = 0;
    obj.v *= 1 - obj.breakV*dt;
  }

  //
  // projectile
  //

  // adjust v based on a
  obj.v = Math.min(obj.maxV, obj.v + obj.a*dt);

  // apply drag to v
  obj.v *= 1 - obj.dragV*dt;

  // if the car is not on some part of the track, then apply the off-track drag
  if(!f5.registry.track.some(function(t) { return Math.abs(t.x-obj.x) < t.radius && Math.abs(t.y-obj.y) < t.radius; }))
    obj.v *= 1 - obj.offTrackV*dt;

  // apply drag to boost
  obj.boostX *= 1 - obj.dragBoost*dt;
  obj.boostY *= 1 - obj.dragBoost*dt;

  // adjust r based on turn and speed
  obj.r += obj.turn * dt * obj.v/obj.maxV;
  // update x/y based on v/r
  obj.x += Math.cos(obj.r) * obj.v*dt + obj.boostX*dt;
  obj.y += Math.sin(obj.r) * obj.v*dt + obj.boostY*dt;
};