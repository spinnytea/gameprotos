'use strict';
var _ = require('../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.tdshooter.behaviors.projectile', [])
  .service('gameprotos.tdshooter.behaviors.projectile', Projectile).name;

function Projectile() {
  Behavior.call(this);
}
util.inherits(Projectile, Behavior);

Projectile.prototype.init = function(obj, f5, config) {
  _.merge(obj, {
    speed: 800
  }, config);
};

Projectile.prototype.update = function(obj, f5, dt) {
  dt /= 1e3;

  obj.x += Math.cos(obj.r) * obj.speed * dt;
  obj.y += Math.sin(obj.r) * obj.speed * dt;
};