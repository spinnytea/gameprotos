'use strict';
var _ = require('../../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.behaviors.collision.point', [])
  .service('gameprotos.2dplatformer.ragdoll.behaviors.collision.point', Collision).name;

function Collision() {
  Behavior.call(this);
}
util.inherits(Collision, Behavior);

Collision.prototype.init = function(obj, f5, config) {
  if(!config.collide_with)
    throw new Error('collision must specify a "collide_with"');

  _.merge(obj, {
    // TODO are there other config options?
  }, config);
};

// 8 states that we can boil down to 3
// - each of the less/middle/more can be triggered independently
// - we need to pick one to represent it
function pickState(piecewise) {
  if(piecewise.middle.length) {
    if(piecewise.less) {
      if(piecewise.more) {
        return 'middle';
      } else {
        return 'less';
      }
    } else {
      if(piecewise.more) {
        return 'more';
      } else {
        return 'middle';
      }
    }
  } else {
    if(piecewise.less) {
      if(piecewise.more) {
        return 'middle';
      } else {
        return 'less';
      }
    } else {
      if(piecewise.more) {
        return 'more';
      } else {
        return 'none';
      }
    }
  }
}

Collision.prototype.update = function(obj, f5, dt) {
  dt /= 1e3;

  obj.collide_with.forEach(function(type) {
    f5.registry[type].forEach(function(thing) {

      // check for collisions with this thing
      var piecewise_x = { less: 0, middle: [], more: 0, dl: 0, dm: 0 };
      var piecewise_y = { less: 0, middle: [], more: 0, dl: 0, dm: 0 };
      obj.collision_points.forEach(function(p) {
        var x = p.x;
        if(x < thing.x)
          piecewise_x.less++;
        else if(x > thing.x + thing.width)
          piecewise_x.more++;
        else {
          piecewise_x.middle.push(p);
          piecewise_x.dl = Math.max(piecewise_x.dl, x - thing.x);
          piecewise_x.dm = Math.max(piecewise_x.dm, thing.x + thing.width - x);
        }

        var y = p.y;
        if(y < thing.y)
          piecewise_y.less++;
        else if(y > thing.y + thing.height)
          piecewise_y.more++;
        else {
          piecewise_y.middle.push(p);
          piecewise_y.dl = Math.max(piecewise_y.dl, y - thing.y);
          piecewise_y.dm = Math.max(piecewise_y.dm, thing.y + thing.height - y);
        }
      });

      if((piecewise_x.middle.length || (piecewise_x.less && piecewise_x.more)) && (piecewise_y.middle.length || (piecewise_y.less && piecewise_y.more))) {
        // handle the collisions
        // XXX only do x or y collision logic; not both
        // - this may not be optimal; there must be a better way to determine which one to do
        // - if we handle both, you get some really weird edge cases
        if(piecewise_x.less + piecewise_x.more > piecewise_y.less + piecewise_y.more) {
          switch(pickState(piecewise_x)) {
            case 'less':
              if(obj.movement.x.v > 0)
                obj.movement.x.v = 0;
              obj.x -= piecewise_x.dl;
              break;
            case 'more':
              if(obj.movement.x.v < 0)
                obj.movement.x.v = 0;
              obj.x += piecewise_x.dm;
              break;

            // if the collision is "in the middle", then the other collision will have something (probably)
            // if this really is in the middle, then there isn't anything we can or should do in this direction
            case 'middle':
            // at least one of each piecewise will be true
            // so this won't be none
            // in any case, if there is no collision, there is nothing to do
            case 'none':
              break;
          }
        } else {
          switch(pickState(piecewise_y)) {
            case 'less':
              if(obj.movement.y.v > 0)
                obj.movement.y.v = 0;
              obj.y -= piecewise_y.dl;
              piecewise_y.middle.forEach(function(p) {
                if(p.collide.top) p.collide.top(p);
              });
              break;
            case 'more':
              if(obj.movement.y.v < 0)
                obj.movement.y.v = 0;
              obj.y += piecewise_y.dm;
              break;

            // if the collision is "in the middle", then the other collision will have something (probably)
            // if this really is in the middle, then there isn't anything we can or should do in this direction
            case 'middle':
            // at least one of each piecewise will be true
            // so this won't be none
            // in any case, if there is no collision, there is nothing to do
            case 'none':
              break;
          }
        }

        // TODO handle the case for pickState === [middle, middle]
        // - is there a difference for "completely inside" vs "completely surrounding"?
      }

    });
  });
};