'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dracing.behaviors.car.waypoint_autopilot', [])
  .service('gameprotos.2dracing.behaviors.car.waypoint_autopilot', Autopilot).name;

function Autopilot() {
  Behavior.call(this);
}
util.inherits(Autopilot, Behavior);

Autopilot.prototype.init = function(obj, f5, config) {
  _.merge(obj, { autopilot: _.merge({
    target_index: 4
  }, config) });
};

Autopilot.prototype.update = function(obj, f5) {
  // always move forward
  obj.controls.gas = true;

  // calculate if we need to turn
  // TODO do fancier calculations to prevent over-steering
  var target = f5.registry.track[obj.autopilot.target_index];
  var d = diff(Math.atan2(target.y-obj.y, target.x-obj.x), obj.r);
  obj.controls.right = (d > 0);
  obj.controls.left = (d < 0);

  // once we get to one waypoint, then target the next one
  if(Math.sqrt(Math.pow(target.y-obj.y, 2) + Math.pow(target.x-obj.x, 2)) < target.radius)
    obj.autopilot.target_index = (obj.autopilot.target_index+1)%f5.registry.track.length;
};

function diff(f, i) {
  var d = f-i;
  // TODO can I do math to remove the while loops
  //  - what about d % (Math.PI*2)
  //  - do I need to convert to degrees first?
  while(d > Math.PI) d -= Math.PI*2;
  while(d < -Math.PI) d += Math.PI*2;
  return d;
}