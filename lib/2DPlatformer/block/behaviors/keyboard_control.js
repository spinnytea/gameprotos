'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dplatformer.block.behaviors.keyboard', [])
  .service('gameprotos.2dplatformer.block.behaviors.keyboard', Keyboard).name;

function Keyboard() {
  Behavior.call(this);
}
util.inherits(Keyboard, Behavior);

Keyboard.prototype.init = function(obj, f5, config) {
  if(!config.hasOwnProperty('left')) throw new Error('must defined a key for "left"');
  if(!config.hasOwnProperty('right')) throw new Error('must defined a key for "right"');
  if(!config.hasOwnProperty('jump')) throw new Error('must defined a key for "jump"');

  _.merge(obj, { keys: config });
};

Keyboard.prototype.update = function(obj, f5) {
  obj.controls.left = f5.input.keys[obj.keys.left];
  obj.controls.right = f5.input.keys[obj.keys.right];
  obj.controls.jump = f5.input.keys[obj.keys.jump];
};