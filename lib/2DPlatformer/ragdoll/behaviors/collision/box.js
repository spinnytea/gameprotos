'use strict';
var _ = require('../../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.behaviors.collision.box', [])
  .service('gameprotos.2dplatformer.ragdoll.behaviors.collision.box', Collision).name;

function Collision() {
  Behavior.call(this);
}
util.inherits(Collision, Behavior);

Collision.prototype.init = function(obj, f5, config) {
  if(!config.collide_with)
    throw new Error('collision must specify a "collide_with"');

  _.merge(obj, {
    // TODO put bounce/collisionDrag on the collision target
    bounce: 10,
    collisionDrag: 4
  }, config);
};

Collision.prototype.update = function(obj, f5, dt) {
  dt /= 1e3;

  obj.collide_with.forEach(function(type) {
    f5.registry[type].forEach(function(thing) {

      if(obj.x + obj.width  > thing.x && obj.x < thing.x + thing.width &&
        obj.y + obj.height > thing.y && obj.y < thing.y + thing.height) {
        // do collision

        var closest = [
          { tag: 'top', dist: Math.abs(obj.y+obj.height-thing.y) },
          { tag: 'bottom', dist: Math.abs(thing.y+thing.height-obj.y) },
          { tag: 'left', dist: Math.abs(obj.x+obj.width-thing.x) },
          { tag: 'right', dist: Math.abs(thing.x+thing.width-obj.x) }
        ].reduce(function(prev, curr) {
            if(curr.dist < prev.dist)
              return curr;
            return prev;
          });

        switch(closest.tag) {
          case 'top':
            if(obj.movement.y.v > 0)
              obj.movement.y.v = 0;
            obj.y = thing.y - obj.height;
            obj.movement.y.v -= obj.bounce;
            obj.movement.x.v *= 1 - obj.collisionDrag*dt;
            break;
          case 'left':
            if(obj.movement.x.v > 0)
              obj.movement.x.v = 0;
            obj.x = thing.x - obj.width;
            obj.movement.x.v -= obj.bounce;
            obj.movement.y.v *= 1 - obj.collisionDrag*dt;
            break;
          case 'right':
            if(obj.movement.x.v < 0)
              obj.movement.x.v = 0;
            obj.x = thing.x + thing.width;
            obj.movement.x.v += obj.bounce;
            obj.movement.y.v *= 1 - obj.collisionDrag*dt;
            break;
        }
      }

    });
  });
};