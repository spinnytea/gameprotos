'use strict';
var _ = require('../../../lodashAdapter');
var states = require('./states/player_states');

var SHOW_COLLISION = false;
module.exports = Player;

// a model of a character with limbs
function Player(config) {
  config = config || {};

  var SCALE = config.scale || 0.75;
  var PLAYER = this;
  this.state = config.state?states[config.state]:states.stand;

  this.x = config.x || 800;
  this.y = config.y || 500;
  this.r = 0;

  // the player isn't a box, nor can one really be calculated
  // (we could get the min/max x/y of each collision point)
  // it is a very free-form shape
  // these points represent the out-most extremities of the sprites
  //
  // Note:
  //  collision points need to be defined before the body
  //  this way, when SHOW_COLLISION is true, then the points will appear above everything
  this.collision_points = [];

  // the back is the anchor
  // the head hangs off the top
  // the upperarm hangs off the middle
  // the forearm hangs off the bottom of the upperarm
  // the thigh hangs off the bottom
  // the calf hangs off the bottom of the thigh
  //
  // okay, really, the back is attached to the player so the back can rotate correctly
  // (and we can animate it normally if we want)
  this.body = {
    front_forearm: {
      isFront: true,
      myR: 0,
      width: 117 * SCALE,
      height: 229 * SCALE,
      image: 'player.forearm'
    },
    front_upperarm: {
      isFront: true,
      myR: 0,
      width: 81 * SCALE,
      height: 182 * SCALE,
      image: 'player.upperarm'
    },
    front_thigh: {
      isFront: true,
      myR: 0,
      width: 129 * SCALE,
      height: 286 * SCALE,
      image: 'player.thigh'
    },
    front_calf: {
      isFront: true,
      myR: 0,
      width: 176 * SCALE,
      height: 238 * SCALE,
      image: 'player.calf'
    },

    head: {
      myR: 0,
      width: 416 * SCALE,
      height: 365 * SCALE,
      image: 'player.head'
    },
    back: {
      isFront: false,
      myR: 0,
      width: 173 * SCALE,
      height: 330 * SCALE,
      image: 'player.body'
    },

    back_thigh: {
      isFront: false,
      myR: 0,
      width: 129 * SCALE,
      height: 286 * SCALE,
      image: 'player.thigh'
    },
    back_calf: {
      isFront: false,
      myR: 0,
      width: 176 * SCALE,
      height: 238 * SCALE,
      image: 'player.calf'
    },
    back_forearm: {
      isFront: false,
      myR: 0,
      width: 117 * SCALE,
      height: 229 * SCALE,
      image: 'player.forearm'
    },
    back_upperarm: {
      isFront: false,
      myR: 0,
      width: 81 * SCALE,
      height: 182 * SCALE,
      image: 'player.upperarm'
    }
  };

  attach(this, this.body.back, { x: 0, y: 0 }, { x: 80 * SCALE, y: 140 * SCALE });
  attach(this.body.back, this.body.head, { x: 80 * SCALE, y: 20 * SCALE }, { x: 220 * SCALE, y: 355 * SCALE });

  attach(this.body.back, this.body.front_thigh, { x: 70 * SCALE, y: 270 * SCALE }, { x: 56 * SCALE, y: 59 * SCALE });
  attach(this.body.front_thigh, this.body.front_calf, { x: 65 * SCALE, y: 245 * SCALE }, { x: 55 * SCALE, y: 50 * SCALE });
  attach(this.body.back, this.body.back_thigh, { x: 70 * SCALE, y: 270 * SCALE }, { x: 56 * SCALE, y: 59 * SCALE });
  attach(this.body.back_thigh, this.body.back_calf, { x: 65 * SCALE, y: 245 * SCALE }, { x: 55 * SCALE, y: 50 * SCALE });

  attach(this.body.back, this.body.front_upperarm, { x: 75 * SCALE, y: 80 * SCALE }, { x: 40 * SCALE, y: 45 * SCALE });
  attach(this.body.front_upperarm, this.body.front_forearm, { x: 35 * SCALE, y: 135 * SCALE }, { x: 54 * SCALE, y: 44 * SCALE });
  attach(this.body.back, this.body.back_upperarm, { x: 75 * SCALE, y: 80 * SCALE }, { x: 40 * SCALE, y: 45 * SCALE });
  attach(this.body.back_upperarm, this.body.back_forearm, { x: 35 * SCALE, y: 135 * SCALE }, { x: 54 * SCALE, y: 44 * SCALE });

  // add some points that will collide with the world
  function runningBounce(p) {
    if(PLAYER.state === states.run) {
      // XXX this could do some strange things when switching between animations
      PLAYER.movement.y.v -= 5 * p.delta.y;
    } else if(PLAYER.state === states.frolic) {
        PLAYER.movement.y.v -= 350;
    }
  }
  [
    { to: 'calf', anchor: { x: 80 * SCALE, y: 40 * SCALE } },
    { to: 'calf', anchor: { x: 50 * SCALE, y: 220 * SCALE } },
    { to: 'calf', anchor: { x: 120 * SCALE, y: 220 * SCALE }, top: runningBounce },

    { to: 'thigh', anchor: { x: 10 * SCALE, y: 100 * SCALE } },
    { to: 'thigh', anchor: { x: 35 * SCALE, y: 200 * SCALE } },

    { to: 'forearm', anchor: { x: 25 * SCALE, y: 55 * SCALE } },
    { to: 'forearm', anchor: { x: 60 * SCALE, y: 215 * SCALE } },

    { to: PLAYER.body.back, anchor: { x: 35 * SCALE, y: 250 * SCALE } },
    { to: PLAYER.body.back, anchor: { x: 25 * SCALE, y: 100 * SCALE } },
    { to: PLAYER.body.back, anchor: { x: 140 * SCALE, y: 80 * SCALE } },
    { to: PLAYER.body.back, anchor: { x: 140 * SCALE, y: 210 * SCALE } },

    { to: PLAYER.body.head, anchor: { x: 110 * SCALE, y: 260 * SCALE } },
    { to: PLAYER.body.head, anchor: { x: 135 * SCALE, y: 30 * SCALE } },
    { to: PLAYER.body.head, anchor: { x: 225 * SCALE, y: 10 * SCALE } },
    { to: PLAYER.body.head, anchor: { x: 400 * SCALE, y: 180 * SCALE } },
    { to: PLAYER.body.head, anchor: { x: 320 * SCALE, y: 310 * SCALE } }
  ].forEach(function(c) {
    var point = { myR: 0, delta: {}, collide: { top: c.top } };
    if(SHOW_COLLISION) {
      point.radius = 20 * (1 - (1 - SCALE) * 0.8);
      point.image = 'dot';
    }
    if(typeof c.to === 'string') {
      var point2 = _.merge({}, point);

      attach(PLAYER.body['front_'+c.to], point, c.anchor, {x:0,y:0});
      attach(PLAYER.body['back_'+c.to], point2, c.anchor, {x:0,y:0});
      PLAYER.collision_points.push(point);
      PLAYER.collision_points.push(point2);
    } else {
      attach(c.to, point, c.anchor, {x:0,y:0});
      PLAYER.collision_points.push(point);
    }
  });

  this.$behavior = [
    {
      service: 'gameprotos.2dplatformer.ragdoll.behaviors.animate',
      config: { animationSpeed: this.state.animation.speed }
    }, {
      service: 'gameprotos.2dplatformer.ragdoll.behaviors.movement'
    }, {
      service: 'gameprotos.2dplatformer.ragdoll.behaviors.keyboard',
      config: {
        left: 'left',
        right: 'right',
        jump: 'space'
      }
    }, {
      service: 'gameprotos.2dplatformer.ragdoll.behaviors.collision.point',
      config: { collide_with: ['platforms'] }
    }
  ];
}

// attach one body part to another
function attach(to, obj, anchor, bnchor) {
  if(obj.hasOwnProperty('x')) throw new Error('attached part cannot define x');
  if(obj.hasOwnProperty('y')) throw new Error('attached part cannot define y');
  if(obj.hasOwnProperty('r')) throw new Error('attached part cannot define r');
  if(!obj.hasOwnProperty('myR')) throw new Error('attached part must define myR');

  Object.defineProperty(obj, 'r', { get: function() {
    return to.r + obj.myR;
  } });
  Object.defineProperty(obj, 'x', { get: function() {
    return to.x +
      (Math.cos(to.r)*anchor.x - Math.sin(to.r)*anchor.y) +
      -(Math.cos(obj.r)*bnchor.x - Math.sin(obj.r)*bnchor.y)
      ;
  } });
  Object.defineProperty(obj, 'y', { get: function() {
    return to.y +
      (Math.cos(to.r)*anchor.y + Math.sin(to.r)*anchor.x) +
      -(Math.cos(obj.r)*bnchor.y + Math.sin(obj.r)*bnchor.x)
      ;
  } });
}

Player.prototype.animate = function(dt) {
  var animation = this.state.animation;
  var body = this.body;
  var animationUpdate = this.animationUpdate;
  var animationTime = this.animationTime;

  var player = this;

  // record the values before, so we can do math after
  this.collision_points.forEach(function(p) {
    p.delta.y = p.y-player.y;
  });


  animationUpdate(body.back, animation.back.myR);
  animationUpdate(body.head, animation.head.myR);


  // left foot first
  // (back foot has initial neg rotation)
  // (front foot has initial pos rotation)
  animationUpdate(body.front_thigh, Math.sin(animationTime), animation.thigh);
  animationUpdate(body.back_thigh, Math.sin(animationTime+Math.PI), animation.thigh);

  animationUpdate(body.front_calf, -Math.cos(animationTime), animation.calf);
  animationUpdate(body.back_calf, -Math.cos(animationTime+Math.PI), animation.calf);


  animationUpdate(body.front_upperarm, Math.sin(animationTime+Math.PI), animation.upperarm);
  animationUpdate(body.back_upperarm, Math.sin(animationTime), animation.upperarm);

  animationUpdate(body.front_forearm, Math.sin(animationTime+Math.PI), animation.forearm);
  animationUpdate(body.back_forearm, Math.sin(animationTime), animation.forearm);


  // do the math after using the recorded as the base
  this.collision_points.forEach(function(p) {
    p.delta.y = ((p.y-player.y)-p.delta.y)/dt;
  });
};

Player.prototype.changeState = function(newState) {
  if(this.state === newState)
    return;

  this.animationSpeed = newState.animation.speed;
  this.state = newState;
  this.animationAsymmetricFlip = this.animationTime > Math.PI/2 && this.animationTime < 3*Math.PI/2;
};
