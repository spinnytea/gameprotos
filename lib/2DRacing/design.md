

## Track

Waypoints define the track

* these can be used for a number of things
* use them for AI

Ways to think about car distance from the track

* http://en.wikipedia.org/wiki/B%C3%A9zier_curve
* http://mathworld.wolfram.com/Point-LineDistance2-Dimensional.html

For now, just use the "discrete" approach.
Put the track into a grid pattern; the grid should be slightly smaller than track pieces.
As long as the car is on a track piece, then it will "move fast"; if it leaves the track, then it will "move slow".

## Definitions

Objects are a bag of behaviors

Car Movement is separate from keyboard controls so we can add an AI to use the controls