'use strict';
var _ = require('../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.tdshooter.behaviors.collide', [])
  .service('gameprotos.tdshooter.behaviors.collide', Collide).name;

function Collide() {
  Behavior.call(this);
}
util.inherits(Collide, Behavior);

Collide.prototype.init = function(obj, f5, config) {
  _.merge(obj, { collide_with: config });
};

Collide.prototype.update = function(obj, f5) {
  var bullet;

  obj.collide_with.some(function(which) {
    var bullets = f5.registry[which];

    bullets.forEach(function(b) {
      if(b.health > 0 && distance(obj, b) < obj.radius + b.radius) {
        bullet = b;
      }
    });

    if(bullet) {
      f5.resource.sound['collide'].play();
      var beforeHealth = obj.health;
      obj.health -= bullet.health;
      bullet.health -= beforeHealth;
      return true;
    }

    return false;
  });
};

function distance(a, b) {
  return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
}
