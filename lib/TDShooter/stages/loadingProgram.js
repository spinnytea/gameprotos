'use strict';
var util = require('util');
var Stage = require('force5/core/stage');
var factory = require('../game_object_factory');

module.exports = require('angular').module('gameprotos.tdshooter.loadingProgram', [])
  .service('gameprotos.tdshooter.loadingProgram', PStage).name;

function PStage() {
  Stage.call(this);
}
util.inherits(PStage, Stage);

PStage.prototype.init = function(f5) {
  f5.resource.loadImage('player.ship', '/resource/TDShooter/player/ship.png');
  f5.resource.loadImage('player.bullet', '/resource/TDShooter/player/bullet.png');
  f5.resource.loadImage('enemy.ship', '/resource/TDShooter/enemy/ship.png');
  f5.resource.loadImage('enemy.bullet', '/resource/TDShooter/enemy/bullet.png');

  f5.resource.loadSound('collide', ['/resource/TDShooter/collide.wav']);
  f5.resource.loadSound('lap', ['/resource/TDShooter/lap.wav']);
  f5.resource.loadSound('death', ['/resource/TDShooter/death.wav']);
  f5.resource.loadSound('enemy.fire', ['/resource/TDShooter/enemy/fire.wav']);
  f5.resource.loadSound('player.fire', ['/resource/TDShooter/player/fire.wav']);
  // TODO Music

  f5.registry.enemies = [];
  f5.registry.enemyBullets = [];
  f5.registry.players = [ factory.player() ];
  f5.registry.playerBullets = [];

  f5.registry.stats = {
    laps: 0
  };
};

PStage.prototype.enter = function(f5) {
  f5.changeStage('gameprotos.tdshooter.stages.stage1');
};