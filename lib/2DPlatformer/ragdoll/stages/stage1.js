'use strict';
var util = require('util');
var PStage = require('./pstage');
var factory = require('../game_object_factory');
var Player = require('../objects/player');
var states = require('../objects/states/player_states');

module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.stages.stage1', [])
  .service('gameprotos.2dplatformer.ragdoll.stages.stage1', Stage).name;

function Stage() {
  PStage.call(this);
}
util.inherits(Stage, PStage);

Stage.prototype.enter = function(f5) {

  f5.registry.player = new Player({
    x: 350,
    y: 500,
    scale: 0.15
  });

  f5.registry.platforms = [];
  var i;
  for(i=0; i<30; i++)
    f5.registry.platforms.push(factory.platform({ x: i*500, y: 700 }));
  for(i=0; i<10; i++)
    f5.registry.platforms.push(factory.platform({ x: i*1500+600, y: 800 }));

  f5.view = {
    x: 0,
    y: 400,
    width: 960,
    height: 540
  };
};

Stage.prototype.leave = function(f5) {
  delete f5.view;
};

Stage.prototype.update = function(f5) {
  var player = f5.registry.player;
  if(!player.movement) return;

  f5.view.x = player.x - 480;

  // aside from the fact that this is not the right place to handle state transitions...
  // is this the right way to handle the states?
  // is it soley based on movement speed?
  // what about... movement types?
  // fly, move, idle
  // - walk, run, frolic are move
  // - jump, fall are flying
  // the idea being that there are triggers that get you from one state into the next
  // then you can move freely within a type based on other parameters
  // does this complicate things?
  // is movement really enough?
  //
  // for these 5 states, it seems to be
  // how do you enter frolic?
  // what about doing a jig while you idle?
  //
  // when switching between jump and fall, it goes through run
  // during the run animation, we bounce quite a bit, but we aren't switching states even though we are falling a little
  if(player.movement.y.v < -200) {
    f5.registry.player.changeState(states.jump);
  } else if(player.movement.y.v > 200) {
    f5.registry.player.changeState(states.fall);
  } else if(player.movement.x.v > 200) {
    if(f5.input.keys.shift)
      f5.registry.player.changeState(states.frolic);
    else
      f5.registry.player.changeState(states.run);
  } else if(player.movement.x.v > 10) {
    f5.registry.player.changeState(states.walk);
  } else if(player.movement.x.v < -10) {
    f5.registry.player.changeState(states.walk);
  } else {
    f5.registry.player.changeState(states.stand);
  }
};
