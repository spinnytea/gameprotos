'use strict';
module.exports = require('angular').module('gameprotos.2dplatformer.block.behaviors', [
  require('./game_object'),
  require('./keyboard_control'),
  require('./movement'),
]).name;