'use strict';
module.exports = require('angular').module('gameprotos.2dplatformer.block.stages', [
  require('./demo/sheet'),
  require('./demo/first'),
  require('./loadingProgram'),
]).name;