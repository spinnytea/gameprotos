'use strict';
module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll', [
  require('./behaviors'),
  require('./stages')
]).name;