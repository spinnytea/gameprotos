'use strict';
var State = require('./player_state');

module.exports = new State({
  animation: {
    speed: 3,

    thigh: {
      front: { amp: 0.03, off: 0.4 },
      back: { amp: 0.03, off: -1.5 }
    },
    calf: {
      front: { amp: 0.03, off: 0.2 },
      back: { amp: 0.03, off: 1.7 }
    },

    upperarm: {
      front: { amp: 0.03, off: -2.4 },
      back: { amp: 0.03, off: 0.9 }
    },
    forearm: {
      front: { amp: 0.03, off: -0.2 },
      back: { amp: 0.03, off: -0.6 }
    },

    head: { myR: -0.75 },
    back: { myR: 0.05 }
  }
});