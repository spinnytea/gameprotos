'use strict';
module.exports = require('angular').module('gameprotos.2dplatformer.block', [
  require('./behaviors'),
  require('./stages')
]).name;