'use strict';
var _ = require('../../lodashAdapter');

// create a new object for the registry
// this is a bag of behaviors
// behaviors: { service: config, service: config }
exports.construct = function(behaviors) {
  return {
    $behavior: _.map(behaviors, function(value, key) {
      return { service: key, config: value };
    })
  };
};

exports.platform = function(config) {
  return exports.construct({
    'gameprotos.2dplatformer.ragdoll.behaviors.game_object': {
      type: 'platforms',

      x: config.x,
      y: config.y,
      width: 500,
      height: 200,
      image: 'platform'
    }
  });
};