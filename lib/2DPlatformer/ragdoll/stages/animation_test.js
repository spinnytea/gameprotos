'use strict';
var util = require('util');
var PStage = require('./pstage');
var Player = require('../objects/player');
var states = require('../objects/states/player_states');

module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.stages.animation_test', [])
  .service('gameprotos.2dplatformer.ragdoll.stages.animation_test', Stage).name;

function Stage() {
  PStage.call(this);
}
util.inherits(Stage, PStage);

Stage.prototype.enter = function(f5) {
  f5.registry.player = new Player({state: 'run'});
  f5.registry.player.$behavior = f5.registry.player.$behavior.filter(function(b) {
    return b.service === 'gameprotos.2dplatformer.ragdoll.behaviors.animate';
  });

  this.change = true;
  this.templateUrl = '/template/2DPlatformer/ragdoll/animation_test.html';
};

Stage.prototype.update = function(f5) {
  if(f5.input.keys.space) {
    if(this.change) {
      switch(f5.registry.player.state) {
        case states.stand:
          f5.registry.player.changeState(states.walk);
          break;
        case states.walk:
          f5.registry.player.changeState(states.run);
          break;
        case states.frolic:
          f5.registry.player.changeState(states.run);
          break;
        case states.run:
          f5.registry.player.changeState(states.jump);
          break;
        case states.jump:
          f5.registry.player.changeState(states.fall);
          break;
        case states.fall:
          f5.registry.player.changeState(states.stand);
          break;
      }
    }

    this.change = false;
  } else {
    this.change = true;
  }

  if(f5.input.keys.shift) {
    f5.registry.player.changeState(states.frolic);
  }
};
