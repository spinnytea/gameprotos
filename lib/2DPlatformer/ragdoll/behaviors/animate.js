'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.behaviors.animate', [])
  .service('gameprotos.2dplatformer.ragdoll.behaviors.animate', Animate).name;

function Animate() {
  Behavior.call(this);
}
util.inherits(Animate, Behavior);

Animate.prototype.init = function(obj, f5, config) {
  // the each cycle is an equation based on elapsed time
  // the animation step itself will adjust the position to move towards this cycle
  _.merge(obj, {
    animationTime: 0,
    animationSpeed: 1,
    animationAsymmetricFlip: false
  }, config);

  // @param part: the body part, the thing with a rotation (myR/isFront)
  // @param target: the target r, basically the sin/cos
  // @param mods: the state's animation modifiers (amp/off; front/back amp/off)
  obj.animationUpdate = function(part, target, mods) {
    if(mods) {
      // asymmetric animations have a front/back
      if(mods.front) {
        if(part.isFront !== obj.animationAsymmetricFlip) {
          target = target*mods.front.amp + mods.front.off;
        } else {
          target = target*mods.back.amp + mods.back.off;
        }
      } else {
        // symmetric animations just have amp and off directly
        target = target*mods.amp + mods.off;
      }
    }

    // this isn't good jerk/acc/vel logic
    // it's very calculated, but it's goal is to make the animations/animation changes fluid
    part.myR += (target-part.myR)*0.2;
  };
};

Animate.prototype.update = function(obj, f5, dt) {
  obj.animationTime += dt/1e3*obj.animationSpeed;
  if(obj.animationTime > Math.PI*2)
    obj.animationTime -= Math.PI*2;
  obj.animate(dt/1e3*obj.animationSpeed);
};