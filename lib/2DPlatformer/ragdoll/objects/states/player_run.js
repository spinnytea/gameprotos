'use strict';
var State = require('./player_state');

module.exports = new State({
  animation: {
    speed: 7,

    thigh: { amp: 0.9, off: -0.5 },
    calf: { amp: 1, off: 1 },

    upperarm: { amp: 1, off: -0.1 },
    forearm: { amp: 0.7, off: -0.9 },

    head: { myR: -0.1 },
    back: { myR: 0.2 }
  }
});