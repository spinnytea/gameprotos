'use strict';
var util = require('util');
var PStage = require('./../pstage');
var factory = require('../../game_object_factory');

module.exports = require('angular').module('gameprotos.2dplatformer.block.stages.demo.first', [])
  .service('gameprotos.2dplatformer.block.stages.demo.first', Stage).name;

function Stage() {
  PStage.call(this);
}
util.inherits(Stage, PStage);

Stage.prototype.enter = function(f5) {
  f5.registry.players = [
    factory.player(2, 7, 'player.stand.purple', {
      left: 'left',
      right: 'right',
      jump: 'space'
    })
  ];

  f5.registry.blocks = [];
  var i;
  for(i=15; i>=0; i--)
    f5.registry.blocks.push(factory.block(i, 0, 'platforms.grass'));
  for(i=1; i<8; i++)
    f5.registry.blocks.push(factory.block(15, i, 'platforms.grass'));

  [
    { x: 11, y: 5 },
    { x: 12, y: 6 },
    { x: 10, y: 3 },

    { x: 7, y: 4 },
    { x: 7, y: 5 },

    { x: 4, y: 6 },
    { x: 4, y: 7 },
  ].forEach(function(p) {
    f5.registry.blocks.push(factory.block(p.x, p.y, 'platforms.grass'));
  });

  for(i=1; i<8; i++)
    f5.registry.blocks.push(factory.block(0, i, 'platforms.grass'));
  for(i=15; i>=0; i--)
    f5.registry.blocks.push(factory.block(i, 8, 'platforms.grass'));

  f5.view = {
    x: 0,
    y: 0,
    width: factory.BLOCK_SPACING*16,
    height: factory.BLOCK_SPACING*9
  };
};

Stage.prototype.leave = function(f5) {
  delete f5.view;
};
