'use strict';
module.exports = require('angular').module('gameprotos.tdshooter.behaviors', [
  require('./bounds/boxed'),
  require('./bounds/vertical'),
  require('./enemy/wander'),
  require('./enemy/random_fire'),
  require('./collide'),
  require('./death'),
  require('./game_object'),
  require('./player_keys'),
  require('./projectile')
]).name;