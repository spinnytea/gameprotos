'use strict';
module.exports = require('angular').module('gameprotos.2dracing.behaviors', [
  require('./car/collision'),
  require('./car/keyboard_control'),
  require('./car/movement'),
  require('./car/waypoint_autopilot'),
  require('./game_object')
]).name;