'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dracing.behaviors.car.keyboard', [])
  .service('gameprotos.2dracing.behaviors.car.keyboard', Keyboard).name;

function Keyboard() {
  Behavior.call(this);
}
util.inherits(Keyboard, Behavior);

Keyboard.prototype.init = function(obj, f5, config) {
  if(!config.hasOwnProperty('left')) throw new Error('must defined a key for "left"');
  if(!config.hasOwnProperty('right')) throw new Error('must defined a key for "right"');
  if(!config.hasOwnProperty('gas')) throw new Error('must defined a key for "gas"');
  if(!config.hasOwnProperty('break')) throw new Error('must defined a key for "break"');

  _.merge(obj, { keys: config });
};

Keyboard.prototype.update = function(obj, f5) {
  obj.controls.left = f5.input.keys[obj.keys.left];
  obj.controls.right = f5.input.keys[obj.keys.right];
  obj.controls.gas = f5.input.keys[obj.keys.gas];
  obj.controls.break = f5.input.keys[obj.keys.break];
};