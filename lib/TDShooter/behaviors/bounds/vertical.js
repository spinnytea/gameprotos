'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.tdshooter.behaviors.bounds.vertical', [])
  .service('gameprotos.tdshooter.behaviors.bounds.vertical', Bounds).name;

function Bounds() {
  Behavior.call(this);
}
util.inherits(Bounds, Behavior);

Bounds.prototype.update = function(obj, f5) {
  var array;

  if(obj.y < -2*obj.radius) {
    array = f5.registry[obj.type];
    array.splice(array.indexOf(obj), 1);
  }

  if(obj.y > f5.height+2*obj.radius) {
    array = f5.registry[obj.type];
    array.splice(array.indexOf(obj), 1);
  }
};