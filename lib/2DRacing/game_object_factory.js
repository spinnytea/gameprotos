'use strict';
var _ = require('../lodashAdapter');

// create a new object for the registry
// this is a bag of behaviors
// behaviors: { service: config, service: config }
exports.construct = function(behaviors) {
  return {
    $behavior: _.map(behaviors, function(value, key) {
      return { service: key, config: value };
    })
  };
};

// specify the corners, and we'll figure out the rest
// the track is a discrete grid; use small integers (i.e. 0..20)
// pieces: { x: int, y: int }
exports.track = function(pieces, config) {
  config = _.merge({
    radius: 140,
    spacing: 260
  }, config);

  var track = [];
  function addTrack(x, y, r, image) {
    track.push(exports.construct({
      'gameprotos.2dracing.behaviors.game_object': {
        x: x*config.spacing + config.radius,
        y: y*config.spacing + config.radius,
        r: r,
        radius: config.radius,
        image: image
      }
    }));
  }

  // previous, current, next indices
  var p = pieces.length-1; var c = 0; var n = 1;
  var px; var py; var nx; var ny;
  while(c < pieces.length) {
    px = pieces[c].x - pieces[p].x;
    py = pieces[c].y - pieces[p].y;
    nx = pieces[n].x - pieces[c].x;
    ny = pieces[n].y - pieces[c].y;

    // validate track corner
    if(!(px === 0 ^ py === 0) || !(nx === 0 ^ ny === 0)) {
      /* jshint ignore:start */
      (function(p, c, n) {
        throw new Error('Invalid corner: ' +
            JSON.stringify([p, c, n]) + ' :: ' +
            JSON.stringify(pieces.filter(function(ignore, i) { return i === p || i === c || i === n; }))
        );
      })(p, c, n);
      /* jshint ignore:end */
    }

    // add track section
    var x; var y; var end;
    if(nx > 0) {

      x = pieces[c].x;
      y = pieces[c].y;
      if(py > 0) {
        addTrack(x, y, Math.PI/2, 'track.turn');
      } else if(py < 0) {
        addTrack(x, y, Math.PI, 'track.turn');
      }

      // right straight
      y = pieces[c].y;
      x = pieces[c].x + 1;
      end = pieces[n].x - 1;
      for(;x <= end;x++)
        addTrack(x, y, 0, 'track.straight');

    } else if(nx < 0) {

      x = pieces[c].x;
      y = pieces[c].y;
      if(py > 0) {
        addTrack(x, y, 0, 'track.turn');
      } else if(py < 0) {
        addTrack(x, y, -Math.PI/2, 'track.turn');
      }

      // left straight
      y = pieces[c].y;
      x = pieces[c].x - 1;
      end = pieces[n].x + 1;
      for(;x >= end;x--)
        addTrack(x, y, Math.PI, 'track.straight');

    } else if(ny > 0) {

      x = pieces[c].x;
      y = pieces[c].y;
      if(px > 0) {
        addTrack(x, y, -Math.PI/2, 'track.turn');
      } else if(px < 0) {
        addTrack(x, y, Math.PI, 'track.turn');
      }

      // down straight
      x = pieces[c].x;
      y = pieces[c].y + 1;
      end = pieces[n].y - 1;
      for(;y <= end;y++)
        addTrack(x, y, Math.PI/2, 'track.straight');

    } else if(ny < 0) {

      x = pieces[c].x;
      y = pieces[c].y;
      if(px > 0) {
        addTrack(x, y, 0, 'track.turn');
      } else if(px < 0) {
        addTrack(x, y, Math.PI/2, 'track.turn');
      }

      // up straight
      x = pieces[c].x;
      y = pieces[c].y - 1;
      end = pieces[n].y + 1;
      for(;y >= end;y--)
        addTrack(x, y, -Math.PI/2, 'track.straight');

    }

    // TODO validate track crossing itself

    // update indexes
    c++;
    p = c-1;
    n = (c+1)%pieces.length;
  }

  return track;
};

// fold the challenger cars into a starting grid
exports.car = function(config) {
  var obj = {
    'gameprotos.2dracing.behaviors.game_object': {
      x: config.startX - 60 * config.place,
      y: config.startY + 60 * (config.place%2 - 0.5),
      r: 0,
      radius: 20,
      image: config.image
    },
    'gameprotos.2dracing.behaviors.car.collision': undefined,
    'gameprotos.2dracing.behaviors.car.movement': undefined
  };

  if(config.keys) {
    obj['gameprotos.2dracing.behaviors.car.keyboard'] = config.keys;
  } else {
    obj['gameprotos.2dracing.behaviors.car.waypoint_autopilot'] = undefined;
  }

  return exports.construct(obj);
};