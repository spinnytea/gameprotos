'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

module.exports = require('angular').module('gameprotos.2dplatformer.block.loadingProgram', [])
  .service('gameprotos.2dplatformer.block.loadingProgram', PStage).name;

function PStage() {
  Stage.call(this);
}
util.inherits(PStage, Stage);

PStage.prototype.init = function(f5) {
  f5.resource.loadImage('swatch', '/resource/swatch.png');
  f5.resource.loadImage('dot', '/resource/dot.png');
  f5.resource.loadImage('square', '/resource/square.png');

  // players
  f5.resource.loadImage('player.idle.purple', '/resource/2DPlatformer/block/objects/player/purple_idle.png');
  f5.resource.loadImage('player.stand.purple', '/resource/2DPlatformer/block/objects/player/purple_stand.png');
  f5.resource.loadImage('player.pose.purple', '/resource/2DPlatformer/block/objects/player/purple_pose.png');

  // platforms
  f5.resource.loadImage('platforms.grass', '/resource/2DPlatformer/block/platforms/grass.png');
  f5.resource.loadImage('platforms.wood', '/resource/2DPlatformer/block/platforms/wood.png');
  f5.resource.loadImage('platforms.water', '/resource/2DPlatformer/block/platforms/water.png');
  f5.resource.loadImage('platforms.fire', '/resource/2DPlatformer/block/platforms/fire.png');
  f5.resource.loadImage('platforms.air', '/resource/2DPlatformer/block/platforms/air.png');
};

PStage.prototype.enter = function(f5) {
  f5.changeStage('gameprotos.2dplatformer.block.stages.demo.first');
};