'use strict';
var State = require('./player_state');

module.exports = new State({
  animation: {
    speed: 4,

    thigh: { amp: 0.5, off: -0.2 },
    calf: { amp: 0.5, off: 0.5 },

    upperarm: { amp: 0.5, off: -0.1 },
    forearm: { amp: 0.2, off: 0 },

    head: { myR: -0.1 },
    back: { myR: 0.1 }
  }
});