'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

module.exports = PStage;

function PStage(next_stage) {
  Stage.call(this);

  this.next_stage = next_stage;
}
util.inherits(PStage, Stage);

PStage.prototype.update = function(f5) {
  if(f5.registry.enemies.length === 0 && f5.registry.enemyBullets.length === 0) {
    f5.changeStage(this.next_stage);
  }
};
