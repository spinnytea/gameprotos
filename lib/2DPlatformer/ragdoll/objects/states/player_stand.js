'use strict';
var State = require('./player_state');

module.exports = new State({
  animation: {
    speed: 2,

    thigh: {
      front: { amp: 0, off: 0.03 },
      back: { amp: 0, off: -0.03 }
    },
    calf: { amp: 0, off: 0 },

    upperarm: { amp: 0.05, off: 0.02 },
    forearm: { amp: 0.05, off: -0.05 },

    head: { myR: 0 },
    back: { myR: 0 }
  }
});