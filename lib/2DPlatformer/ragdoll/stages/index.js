'use strict';
module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.stages', [
  require('./loadingProgram'),
  require('./stage1'),
  require('./animation_test')
]).name;