'use strict';
var util = require('util');
var PStage = require('./pstage');
var factory = require('../game_object_factory');

module.exports = require('angular').module('gameprotos.tdshooter.stages.stage1', [])
  .service('gameprotos.tdshooter.stages.stage1', Stage).name;

function Stage() {
  PStage.call(this, 'gameprotos.tdshooter.stages.stage2');
}
util.inherits(Stage, PStage);

Stage.prototype.enter = function(f5) {
  for(var i=0; i<6; i++)
    f5.registry.enemies.push(factory.enemy({
      x: Math.random()*f5.width,
      y: Math.random()*f5.height/2
    }));
};
