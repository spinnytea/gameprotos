'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dplatformer.block.behaviors.game_object', [])
  .service('gameprotos.2dplatformer.block.behaviors.game_object', GameObject).name;

// generic behavior to stuff config options into
// this is a side effect of objects being a bag of behaviors
function GameObject() {
  Behavior.call(this);
}
util.inherits(GameObject, Behavior);

GameObject.prototype.init = function(obj, f5, config) {
  _.merge(obj, {
    r: 0
  }, config);

  if(obj.hasOwnProperty('sprite')) {
    Object.defineProperty(obj.sprite, 'x', { get: function() { return obj.x - obj.sprite.offset_x; } });
    Object.defineProperty(obj.sprite, 'y', { get: function() { return obj.y - obj.sprite.offset_y; } });
  }
};
