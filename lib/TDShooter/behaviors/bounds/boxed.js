'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.tdshooter.behaviors.bounds.boxed', [])
  .service('gameprotos.tdshooter.behaviors.bounds.boxed', Bounds).name;

function Bounds() {
  Behavior.call(this);
  this.$priority = 200;
}
util.inherits(Bounds, Behavior);

Bounds.prototype.update = function(obj, f5) {
  if(obj.y < obj.radius) {
    obj.y = obj.radius;
  }
  if(obj.y > f5.height-obj.radius) {
    obj.y = f5.height-obj.radius;
  }

  if(obj.x < obj.radius) {
    obj.x = obj.radius;
  }
  if(obj.x > f5.width-obj.radius) {
    obj.x = f5.width-obj.radius;
  }
};