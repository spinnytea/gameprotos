'use strict';
var State = require('./player_state');

module.exports = new State({
  animation: {
    speed: 3,

    thigh: {
      front: { amp: 0.03, off: -1.1 },
      back: { amp: 0.03, off: -1.5 }
    },
    calf: {
      front: { amp: 0.03, off: 1.7 },
      back: { amp: 0.03, off: 0.8 }
    },

    upperarm: { amp: 0.03, off: 0.9 },
    forearm: { amp: 0.03, off: -0.6 },

    head: { myR: 0.3},
    back: { myR: 0.3 }
  }
});