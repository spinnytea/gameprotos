'use strict';

module.exports = {
  map: require('lodash/collection/map'),
  merge: require('lodash/object/merge')
};