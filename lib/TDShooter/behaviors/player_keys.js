'use strict';
var _ = require('../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');
var factory = require('../game_object_factory');

module.exports = require('angular').module('gameprotos.tdshooter.behaviors.player_keys', [])
  .service('gameprotos.tdshooter.behaviors.player_keys', PlayerKeys).name;

function PlayerKeys() {
  Behavior.call(this);
}
util.inherits(PlayerKeys, Behavior);

PlayerKeys.prototype.init = function(obj, f5, config) {
  _.merge(obj, {

    hasFired: false,

    move_speed: 500
  }, config);
};

PlayerKeys.prototype.update = function(obj, f5, dt) {
  dt /= 1e3;

  if(f5.input.keys.left) obj.x -= obj.move_speed*dt;
  if(f5.input.keys.right) obj.x += obj.move_speed*dt;
  if(f5.input.keys.up) obj.y -= obj.move_speed*dt;
  if(f5.input.keys.down) obj.y += obj.move_speed*dt;

  if(f5.input.keys.space) {
    if(!obj.hasFired) {
      obj.hasFired = true;
      f5.registry.playerBullets.push(factory.player.bullet(obj));
      f5.resource.sound['player.fire'].play();
    }
  } else {
    obj.hasFired = false;
  }
};