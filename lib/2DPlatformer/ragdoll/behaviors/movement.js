'use strict';
var _ = require('../../../lodashAdapter');
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('gameprotos.2dplatformer.ragdoll.behaviors.movement', [])
  .service('gameprotos.2dplatformer.ragdoll.behaviors.movement', Movement).name;

function Movement() {
  Behavior.call(this);
}
util.inherits(Movement, Behavior);

Movement.prototype.init = function(obj, f5, config) {
  _.merge(obj, {
    controls: {
      left: false,
      right: false,
      jump: false
    },
    movement: {
      x: {
        // acceleration
        a: 1400,

        // velocity
        v: 0,
        dragV: 1.3,
        maxV: 600
      },
      y: {
        // gravity
        a: 800,

        jump: -400,

        // velocity
        v: 0,
        maxV: 400
      }
    }
  }, config);
};

Movement.prototype.update = function(obj, f5, dt) {
  dt /= 1e3;

  //
  // handle controls
  //

  if(obj.controls.left) {
    obj.movement.x.v -= obj.movement.x.a*dt;
  } else if(obj.controls.right) {
    obj.movement.x.v += obj.movement.x.a*dt;
  }

  if(obj.controls.jump) {
    obj.movement.y.v = obj.movement.y.jump;
  }

  //
  // projectile
  //

  // drag on x
  obj.movement.x.v *= 1 - obj.movement.x.dragV*dt;
  // gravity on y
  obj.movement.y.v += obj.movement.y.a*dt;

  // clamp x and y
  obj.movement.x.v = Math.max(-obj.movement.x.maxV, Math.min(obj.movement.x.maxV, obj.movement.x.v));
  obj.movement.y.v = Math.max(-obj.movement.y.maxV, Math.min(obj.movement.y.maxV, obj.movement.y.v));

  obj.x += obj.movement.x.v*dt;
  obj.y += obj.movement.y.v*dt;
};