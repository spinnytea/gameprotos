'use strict';
var util = require('util');
var PStage = require('./../pstage');
var factory = require('../../game_object_factory');

module.exports = require('angular').module('gameprotos.2dplatformer.block.stages.demo.sheet', [])
  .service('gameprotos.2dplatformer.block.stages.demo.sheet', Stage).name;

function Stage() {
  PStage.call(this);
}
util.inherits(Stage, PStage);

Stage.prototype.enter = function(f5) {
  f5.registry.players = [
    factory.player(1, 0, 'player.pose.purple'),
    factory.player(0, 0, 'player.stand.purple'),
    factory.player(-1, 0, 'player.idle.purple')
  ];

  f5.registry.blocks = [
    factory.block(3, 1, 'platforms.air'),
    factory.block(2, 1, 'platforms.fire'),
    factory.block(1, 1, 'platforms.water'),
    factory.block(0, 1, 'platforms.wood'),
    factory.block(-1, 1, 'platforms.grass')
  ];

  f5.view = {
    x: -1000,
    y: -400,
    width: 3840,
    height: 2160
  };
};

Stage.prototype.leave = function(f5) {
  delete f5.view;
};
